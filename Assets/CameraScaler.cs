﻿using System;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace UnityModule.Resolution
{
    [ExecuteInEditMode]
    public class CameraScaler : MonoBehaviour
    {
        public Vector2 targetResolution = new Vector2(800f, 600f);
        public float pixelsToUnits = 100;
        [SerializeField] private Vector2 logicAreaRatio = new Vector2(2960.0f, 1440.0f);
        private Camera _camera;
        private Rect _originViewPort;

        [SerializeField] private Camera logicalCamera;

        // ReSharper disable once MemberCanBePrivate.Global
        // ReSharper disable once ConvertToAutoProperty
        public Camera LogicalCamera
        {
            get => logicalCamera;
            set => logicalCamera = value;
        }

        // ReSharper disable once MemberCanBePrivate.Global
        public CameraPoint CurrentCameraPoint { get; set; } = new CameraPoint();

        // ReSharper disable once MemberCanBePrivate.Global
        public CameraPoint LogicCameraPoint { get; set; } = new CameraPoint();

        protected void Awake()
        {
            _camera = GetComponent<Camera>();
#if !UNITY_EDITOR
            Calculate();
            if (LogicalCamera == null)
            {
                CreateLogicalCamera();
            }
#endif
        }

#if UNITY_EDITOR
        private void Update()
        {
            Calculate();
        }

        private void OnDrawGizmos()
        {
            if (_camera.orthographic)
            {
               DrawGizmos();
            }
        }

        private void DrawGizmos()
        {
            //*** bottom
            Gizmos.color = Color.green;
            Gizmos.DrawIcon(CurrentCameraPoint.BottomLeft, "point.png", false);
            Gizmos.DrawIcon(CurrentCameraPoint.BottomCenter, "point.png", false);
            Gizmos.DrawIcon(CurrentCameraPoint.BottomRight, "point.png", false);
            //*** middle
            Gizmos.DrawIcon(CurrentCameraPoint.MiddleLeft, "point.png", false);
            Gizmos.DrawIcon(CurrentCameraPoint.MiddleCenter, "point.png", false);
            Gizmos.DrawIcon(CurrentCameraPoint.MiddleRight, "point.png", false);
            //*** top
            Gizmos.DrawIcon(CurrentCameraPoint.TopLeft, "point.png", false);
            Gizmos.DrawIcon(CurrentCameraPoint.TopCenter, "point.png", false);
            Gizmos.DrawIcon(CurrentCameraPoint.TopRight, "point.png", false);
            
            Gizmos.DrawLine(CurrentCameraPoint.BottomLeft, CurrentCameraPoint.BottomRight);
            Gizmos.DrawLine(CurrentCameraPoint.BottomRight, CurrentCameraPoint.TopRight);
            Gizmos.DrawLine(CurrentCameraPoint.TopRight, CurrentCameraPoint.TopLeft);
            Gizmos.DrawLine(CurrentCameraPoint.TopLeft, CurrentCameraPoint.BottomLeft);

            #region Area Logic

            //*** bottom
            Gizmos.color = Color.red;
            Gizmos.DrawIcon(LogicCameraPoint.BottomLeft, "point.png", false);
            Gizmos.DrawIcon(LogicCameraPoint.BottomCenter, "point.png", false);
            Gizmos.DrawIcon(LogicCameraPoint.BottomRight, "point.png", false);
            //*** middle
            Gizmos.DrawIcon(LogicCameraPoint.MiddleLeft, "point.png", false);
            Gizmos.DrawIcon(LogicCameraPoint.MiddleCenter, "point.png", false);
            Gizmos.DrawIcon(LogicCameraPoint.MiddleRight, "point.png", false);
            //*** top
            Gizmos.DrawIcon(LogicCameraPoint.TopLeft, "point.png", false);
            Gizmos.DrawIcon(LogicCameraPoint.TopCenter, "point.png", false);
            Gizmos.DrawIcon(LogicCameraPoint.TopRight, "point.png", false);
            
            Gizmos.DrawLine(LogicCameraPoint.BottomLeft, LogicCameraPoint.BottomRight);
            Gizmos.DrawLine(LogicCameraPoint.BottomRight, LogicCameraPoint.TopRight);
            Gizmos.DrawLine(LogicCameraPoint.TopRight, LogicCameraPoint.TopLeft);
            Gizmos.DrawLine(LogicCameraPoint.TopLeft, LogicCameraPoint.BottomLeft);

            #endregion
        }

#endif
        public void CreateLogicalCamera()
        {
            LogicalCamera = new GameObject().AddComponent<Camera>();
            LogicalCamera.backgroundColor = Color.black;
            LogicalCamera.cullingMask = 0;
            LogicalCamera.depth = -100;
            LogicalCamera.farClipPlane = 10;
            LogicalCamera.useOcclusionCulling = false;
            LogicalCamera.allowHDR = false;
            LogicalCamera.clearFlags = CameraClearFlags.Color;
            LogicalCamera.name = "Logical Camera";
            LogicalCamera.orthographic = true;
            float value = 0;
            if (IsLandscape(CurrentCameraPoint))
            {
                value = targetResolution.x / CurrentCameraPoint.Height * LogicCameraPoint.Height;
            }
            else
            {
                value = targetResolution.y;
            }

            // ReSharper disable once RedundantAssignment
            var screenSize = Vector2.zero;
#if UNITY_EDITOR
            screenSize = GetGameView();
#else
            screenSize = new Vector2(Screen.width, Screen.height);
#endif
            LogicalCamera.orthographicSize = Mathf.RoundToInt(value / screenSize.x * screenSize.y) / pixelsToUnits / 2;
            CalculateRectLogicCamera();
        }

        private void Calculate()
        {
            // ReSharper disable once RedundantAssignment
            var screenSize = Vector2.zero;
#if UNITY_EDITOR
            screenSize = GetGameView();
#else
            screenSize = new Vector2(Screen.width, Screen.height);
#endif
            var aspect = _camera.aspect;
            if (IsLandscape(_camera))
            {
                _camera.orthographicSize = Mathf.RoundToInt(targetResolution.x / screenSize.x * screenSize.y) / pixelsToUnits / 2;
            }
            else
            {
                _camera.orthographicSize = Mathf.RoundToInt(targetResolution.y / screenSize.x * screenSize.y) / pixelsToUnits / 2;
            }

            CurrentCameraPoint.Height = 2f * _camera.orthographicSize;
            CurrentCameraPoint.Width = CurrentCameraPoint.Height * aspect;

            if (IsLandscape(CurrentCameraPoint))
            {
                LogicCameraPoint.Height = CurrentCameraPoint.Height / (logicAreaRatio.x / logicAreaRatio.y / aspect);
                LogicCameraPoint.Width = CurrentCameraPoint.Width;
            }
            else
            {
                LogicCameraPoint.Height = CurrentCameraPoint.Height;
                LogicCameraPoint.Width = CurrentCameraPoint.Width * (logicAreaRatio.y / logicAreaRatio.x / aspect);
            }

            var position = _camera.transform.position;
            var cameraX = position.x;
            var cameraY = position.y;

            var leftX = cameraX - CurrentCameraPoint.Width / 2;
            var rightX = cameraX + CurrentCameraPoint.Width / 2;
            var topY = cameraY + CurrentCameraPoint.Height / 2;
            var bottomY = cameraY - CurrentCameraPoint.Height / 2;

            //*** bottom
            CurrentCameraPoint.BottomLeft = new Vector3(leftX, bottomY, 0);
            CurrentCameraPoint.BottomCenter = new Vector3(cameraX, bottomY, 0);
            CurrentCameraPoint.BottomRight = new Vector3(rightX, bottomY, 0);
            //*** middle
            CurrentCameraPoint.MiddleLeft = new Vector3(leftX, cameraY, 0);
            CurrentCameraPoint.MiddleCenter = new Vector3(cameraX, cameraY, 0);
            CurrentCameraPoint.MiddleRight = new Vector3(rightX, cameraY, 0);
            //*** top
            CurrentCameraPoint.TopLeft = new Vector3(leftX, topY, 0);
            CurrentCameraPoint.TopCenter = new Vector3(cameraX, topY, 0);
            CurrentCameraPoint.TopRight = new Vector3(rightX, topY, 0);


            leftX = cameraX - LogicCameraPoint.Width / 2;
            rightX = cameraX + LogicCameraPoint.Width / 2;
            topY = cameraY + LogicCameraPoint.Height / 2;
            bottomY = cameraY - LogicCameraPoint.Height / 2;

            //*** bottom
            LogicCameraPoint.BottomLeft = new Vector3(leftX, bottomY, 0);
            LogicCameraPoint.BottomCenter = new Vector3(cameraX, bottomY, 0);
            LogicCameraPoint.BottomRight = new Vector3(rightX, bottomY, 0);
            //*** middle
            LogicCameraPoint.MiddleLeft = new Vector3(leftX, cameraY, 0);
            LogicCameraPoint.MiddleCenter = new Vector3(cameraX, cameraY, 0);
            LogicCameraPoint.MiddleRight = new Vector3(rightX, cameraY, 0);
            //*** top
            LogicCameraPoint.TopLeft = new Vector3(leftX, topY, 0);
            LogicCameraPoint.TopCenter = new Vector3(cameraX, topY, 0);
            LogicCameraPoint.TopRight = new Vector3(rightX, topY, 0);
        }

        private void CalculateRectLogicCamera()
        {
            var rect = LogicalCamera.rect;
            rect = IsLandscape(CurrentCameraPoint)
                ? new Rect(rect.x, rect.y, rect.width, LogicCameraPoint.Height / CurrentCameraPoint.Height)
                : new Rect(rect.x, rect.y, LogicCameraPoint.Width / CurrentCameraPoint.Width, rect.height);
            LogicalCamera.rect = rect;
        }

        // ReSharper disable once MemberCanBePrivate.Global
        public static bool IsLandscape(Camera cam)
        {
            var h = cam.orthographicSize * 2;
            return h < h * cam.aspect;
        }

        // ReSharper disable once MemberCanBePrivate.Global
        public static bool IsLandscape(CameraPoint point)
        {
            return point.Height < point.Width;
        }

        private Vector2 AnchorBasedOnEnum(CameraAnchor anchor)
        {
            switch (anchor)
            {
                case CameraAnchor.Center: return new Vector2(0.5f, 0.5f);
                case CameraAnchor.Top: return new Vector2(0.5f, 1f);
                case CameraAnchor.Bottom: return new Vector2(0.5f, 0f);
                case CameraAnchor.Left: return new Vector2(0f, 0.5f);
                case CameraAnchor.Right: return new Vector2(1f, 0.5f);
                case CameraAnchor.TopLeft: return Vector2.up;
                case CameraAnchor.TopRight: return Vector2.one;
                case CameraAnchor.BottomLeft: return Vector2.zero;
                case CameraAnchor.BottomRight: return Vector2.right;
                default: return new Vector2(0.5f, 0.5f);
            }
        }

        private static Vector2 GetGameView()
        {
            var T = Type.GetType("UnityEditor.GameView,UnityEditor");
            if (T == null) return Vector2.zero;
            var getSizeOfMainGameView = T.GetMethod("GetSizeOfMainGameView", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
            if (getSizeOfMainGameView == null) return Vector2.zero;
            var resolution = getSizeOfMainGameView.Invoke(null, null);
            return (Vector2) resolution;
        }

        ///// <summary>
        ///// Screen ratio list
        ///// </summary>
        //private static readonly List<Vector2> SizeList = new List<Vector2>()
        //{
        //    new Vector2(16.0f, 12.0f),
        //    new Vector2(16.0f, 10.0f),
        //    new Vector2(16.0f, 9.0f),
        //    new Vector2(16.0f, 8.0f),
        //    new Vector2(16.0f, 7.784f),
        //    new Vector2(16.0f, 7.389f),
        //};
    }

    public enum CameraAnchor
    {
        Center,
        Top,
        Bottom,
        Left,
        Right,
        TopLeft,
        TopRight,
        BottomLeft,
        BottomRight
    }

    public class CameraPoint
    {
        public float Height { get; set; }
        public float Width { get; set; }
        public Vector3 BottomLeft { get; set; }
        public Vector3 BottomCenter { get; set; }
        public Vector3 BottomRight { get; set; }
        public Vector3 MiddleLeft { get; set; }
        public Vector3 MiddleCenter { get; set; }
        public Vector3 MiddleRight { get; set; }
        public Vector3 TopLeft { get; set; }
        public Vector3 TopCenter { get; set; }
        public Vector3 TopRight { get; set; }
    }
}