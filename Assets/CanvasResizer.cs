﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

// ReSharper disable ConvertToAutoProperty
// ReSharper disable once CheckNamespace
namespace UnityModule.Resolution
{
    /// <inheritdoc />
    /// <summary>
    /// Resize Canvas according to screen size
    /// </summary>
    [RequireComponent(typeof(CanvasScaler))]
    [ExecuteInEditMode]
    [PublicAPI]
    public class CanvasResizer : MonoBehaviour
    {
        /// <summary>
        /// Buffer for resolution calculation
        /// </summary>
        /// <remarks>Set the buffer in consideration of the case where either side is the minimum resolution</remarks>
        private const float CALCULATE_RESOLUTION_BUFFER = 10.0f;

        /// <summary>
        /// Default standard resolution
        /// </summary>
        private static readonly Vector2 DefaultStandardResolution = new Vector2(800f, 600f);

        private static readonly float DefaultRatio = 1.777778f;

        /// <summary>
        /// CanvasScaler entity
        /// </summary>
        private CanvasScaler _canvasScaler;

        /// <summary>
        /// CanvasScaler
        /// </summary>
        private CanvasScaler CanvasScaler
        {
            get
            {
                if (_canvasScaler == default(CanvasScaler))
                {
                    _canvasScaler = gameObject.GetComponent<CanvasScaler>();
                }

                return _canvasScaler;
            }
        }

        /// <summary>
        /// Standard resolution entity
        /// </summary>
        [SerializeField] private Vector2 standardResolution = DefaultStandardResolution;

        /// <summary>
        /// Standard resolution
        /// </summary>
        private Vector2 StandardResolution => standardResolution;

        /// <summary>
        /// Resize
        /// </summary>
        /// <remarks>Internally, it only rewrites CanvasScaler.matchWidthOrHeight</remarks>
        public void Resize()
        {
            CanvasScaler.matchWidthOrHeight = CalculateMatchWidthOrHeight();
            var screenSize = new Vector2(Screen.width, Screen.height);
            var ratio = screenSize.x / screenSize.y;
            CanvasScaler.referenceResolution = screenSize.y > screenSize.x
                ? new Vector2(StandardResolution.x, StandardResolution.y / ratio / DefaultRatio)
                : new Vector2(StandardResolution.x * ratio / DefaultRatio, StandardResolution.y);
            CanvasScaler.matchWidthOrHeight = CalculateMatchWidthOrHeight();
        }

        /// <summary>
        /// Unity lifecycle: Start
        /// </summary>
        /// <remarks>
        /// Run Canvas resize process according to screen size at initialization (mainly on real machine)
        /// </remarks>
        protected void OnStart()
        {
            Resize();
        }

#if UNITY_EDITOR

        private Vector2Int CurrentScreenResolution { get; set; } = Vector2Int.zero;

        private void Update()
        {
            if (CurrentScreenResolution.x == Screen.width && CurrentScreenResolution.y == Screen.height)
            {
                return;
            }

            CurrentScreenResolution = new Vector2Int(Screen.width, Screen.height);
            Resize();
        }
#endif

        /// <summary>
        /// Calculate whether to combine Width or Height
        /// </summary>
        /// <returns>Calculation results</returns>
        [SuppressMessage("ReSharper", "RedundantCast")]
        private float CalculateMatchWidthOrHeight()
        {
            var referenceResolution = CanvasScaler.referenceResolution;
            return (float) Screen.height / (float) Screen.width < referenceResolution.y / referenceResolution.x ? 0.0f : 1.0f;
        }

#if UNITY_EDITOR

        /// <summary>
        /// Gizmo color list to draw
        /// </summary>
        /// <remarks>
        /// Cycle this content
        /// </remarks>
        private static readonly List<Color> ColorList = new List<Color>()
        {
            Color.cyan, Color.red, Color.magenta, Color.yellow, Color.green,
        };
        
#endif
    }
}