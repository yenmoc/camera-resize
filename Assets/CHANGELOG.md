# Changelog

## [0.0.3] - 12-11-2019

### open with unity 2019.3.10b

## [0.0.2] - 10-08-2019

* Update package directory structure

## [0.0.1] - 04-08-2019

* Initial version
